const bcrypt = require('bcryptjs');


const salt = bcrypt.genSaltSync(10);

const hash = bcrypt.hashSync('345678', salt);

console.log({salt, hash});

