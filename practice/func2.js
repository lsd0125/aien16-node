
const f1 = a => a*a;

const f2 = function(a){
    return a*a*a;
};


module.exports = {
    f1, 
    f2,
};