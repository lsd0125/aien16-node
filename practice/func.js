
const f1 = a => a*a;

const f2 = function(a){
    return a*a*a;
};


console.log( f1(5) );
console.log( f2(5) );


module.exports = f1;